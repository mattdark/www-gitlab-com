---
layout: handbook-page-toc
title: "GitLab Offboarding"
description: "Offboarding procedures for all stakeholders"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Offboarding Overview

Facilitation of the offboarding process is shared by the GitLab [People Business Partners (PBPs)](https://about.gitlab.com/job-families/people-ops/people-business-partner/), [People Operations Specialists](https://about.gitlab.com/job-families/people-ops/people-operations/), [People Experience Team](https://about.gitlab.com/job-families/people-ops/people-experience-associate/), [Team Member Relations Specialist](https://about.gitlab.com/job-families/people-ops/people-operations/) and various [Tech Stack Provisioners](https://about.gitlab.com/handbook/business-ops/tech-stack-applications/).

If you have any questions about the offboarding process, please review the Offboarding [FAQ page](/handbook/people-group/offboarding/faq.html). 

The process to be followed will be guided by the nature of the offboarding at hand i.e. either **Voluntary** or **Involuntary** - both of which have been described in greater detail below.  Each of the stakeholders listed play a critical role in ensuring that offboarding takes place in a manner that is efficient and best serves the unique needs and sensitivities surrounding the respective team members situation.

## Voluntary Offboarding

A voluntary offboarding occurs when a team member informs his or her manager of a resignation. The choice to leave GitLab was their decision.

If you are a current team member and you are considering resigning from GitLab, we encourage you to speak with your manager, your assigned PBP, or another trusted team member to discuss your reasons for wanting to leave. At GitLab we want to ensure that all issues team members are facing are discussed and resolved before a resignation decision has been made.  This will allow GitLab the ability to address concerns and in return foster a great work environment.

If resignation is the only solution after you have discussed your concerns, please communicate to your manager your intention to resign. We would advise you to review your employment contract for the statutory notice period and determine your last working day. Then you will work with your manager to discuss the handover/transition plan.  If there’s no notice period included in your employment contract we ask that you provide GitLab 2 weeks of notice. Depending on the situation and local laws, GitLab may choose to provide you payment in lieu of notice.  Please review and follow the process below for voluntary resignations.   

### Voluntary Process

1. **Team Member**: Team members are asked to provide an agreed upon notice of their intention to separate from the company to allow a reasonable amount of time to transfer ongoing workloads.
1. **Team Member** If you are employed via one of our [Professional Employer Organization/ Employer of Record and not a GitLab entity](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity), please forward your resignation to the PEO for their process and record keeping. 
1. **Team Member**: The team member should provide a written resignation letter or email notification to their manager.
1. **Team Member**: Please review and action the steps listed on the [Team Member Enablement Offboarding Laptop page](https://about.gitlab.com/handbook/business-technology/team-member-enablement/gitlab-laptop-offboarding/) 
1. **Team Member**: The team member should arrange a meeting with the respective IT Ops team member on their last day (30-60 minutes before the scheduled offboarding time) to complete the laptop wipe:
    - Marc DiSabatino - AMER time zone
    - Paul Laurinavicius - EMEA time zone
    - Steve Ladgrove - APAC time zone
1. **PBP**: Upon receipt of the resignation, the manager will notify the People Business Partner (PBP) by sending a copy of the resignation email/letter.
    * A discussion with the  manager and PBP should also happen if needed to determine what led to the resignation.
    * The PBP will acknowledge the receipt of the resignation with the team member confirming the last working day. Feel free to reference this [example email](https://docs.google.com/document/d/1rZvczqEuyyFDAzjheiF4LpcLuvj9fFd6maPOSDwkYpQ/edit).
    * **If the team member is located in a country with statutory holiday allowance**, the holiday taken should be confirmed with the manager and team member via email and then filed in BambooHR.
    * Once that has been done an Acknowledgement Letter can be prepared, which will depend on the `location` and `employment status` of the team member.
    * A couple of examples can be reviewed here: [GitLab Ltd (UK) Resignation Acknowledgement](https://docs.google.com/document/d/1zV1qnZmjQaNZ3QUjrLOtod-FbboNCPmYdqCLIAc7aos/edit) and [GitLab BV (Netherlands) Resignation Acknowledgement](https://docs.google.com/document/d/1-9hCL2Xs5po4lZ19L2vrcmGxQIYRD-Emyci3F63kt0c/edit). 

    _Note: For GitLab UK team members, payroll can pay up to and including the 5th day of the following month. For example, if a team member's last day is February 2nd, the team member can receive their final pay for this in January's payroll._
1. **PBP**:The PBP will complete the [Offboarding Form](https://forms.gle/HzUAVCgTJ4v5HSSA9), which includes all information needed to process the termination for the team member. Once submitted, a summary of the response will be posted to the `#offboardings` Slack channel so all stakeholders can acknowledge the offboarding. 

The form includes the following fields all of which will guide the People Experience Team in terms of when the process should commence and whether any factors unique to the offboarding in question should be kept in mind e.g. Garden Leave or Day to Turn Off Access.

| Form Element | Guidelines | 
| --- | --- |
| Team Member Name | As Detailed on BambooHR |
| Job Title | As Detailed on BambooHR | 
| Termination Date | As Determined by PBP / Manager and Team Member |
| Garden Leave | Where Applicable (Non-US) |
| Last Working Day | Where Applicable (US) |
| Type of Offboarding | Voluntary or Involuntary | 
| Regrettable vs Non-Regrettable | Further Detail Below |
| Offboarding Reason | Further Detail Below | 
| Eligible for Rehire | Yes / No |
| Eligible for Alumni Channel | Yes / No | 
| Team Member Location | As per BambooHR | 
| Exit Interview Owner | PBP / Specialist |

When completing the form in instances of Voluntary Offboarding i.e. as a result of Team Member Resignation, the People Business Partner along with the Manager of the departing Team Member will need to give consideration to whether the offboarding could be considered Regrettable or Non-Regrettable - the explainations below should be used to guide the outcome:

**Regrettable Offboarding**
- Departure has a significant negative impact on the Company / Customer(s) / Project(s) / Team.
- Team Member was a good and consistent performer both in terms of results and behaviours i.e. upholding the GitLab Values.

**Non-Regrettable Offboarding**
- Departure has a minimal impact on the Company / Customer(s) / Project(s) / Team.
- Team Member has not been meeting expectations due to Conduct / Culture (Values) Misalignment / Performance Concerns.

The table below details the circumstances under which a Team Member will or will not be eligible for future rehire based on the primary reason for offboarding:

| Offboarding Reason | Re-Hire Eligibility |
| ------ | ------ |
| Relocation | Yes |
| Career Development | Yes |
| Elimination of Position | Yes |
| Compensation | Yes |
| Attendance | with Review |
| Values Misalignment | with Review |
| Job Satisfaction | with Review |
| Knowledge, Skills, and Ability | with Review |
| Mutual Decision | with Review |
| Other | with Review |
| Performance | with Review |
| Not a fit for the role | with Review |
| [Conduct](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d)| No |
| [Job Abandonment](https://about.gitlab.com/handbook/people-policies/#job-abandonment) | No |

Offboarding officially commences once an [Offboarding Issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/offboarding.md) has been created by the People Experience Team.  This action is triggered by the submission of an [Offboarding Workflow Form](https://docs.google.com/forms/d/e/1FAIpQLScBBnV7bKKVkqfTk9Aq9sfSB_r85SKTxt0_mC6RhbRwU3DtSA/viewform) by the People Business Partner (PBP) responsible for overseeing the division in which the departing team member is based.

Opening of the offboarding issue which will in turn begin the process of systems de-provisioning will be aligned to the information presented by the People Business Partner within the Offboarding Workflow i.e. Termination Date / Last Working Day.

The People Experience Team endeavours to initiate the process within twelve hours of notification - sooner where possible however in instances of involuntary termination will work with IT Ops to ensure preliminary de-provisioning happens as required.

## Compliance

The People Experience Associates complete a weekly audit of all offboarding issues opened within that specific week and check that all tasks have been completed by all Departments. In the event that tasks are still outstanding, the People Experience Associate will ping the relevant Team Members / Departments within the offboarding issue to call for tasks to be completed.

Once all tasks have been completed, the People Experience Associate will close the offboarding issue and mark as completed in the offboarding tracker.

All offboarding tasks by all Departments need to be completed within 5 days of the offboarding due date. For systems that are more critical and time sensitive, these will be completed within the first 24 hours (example 1Password, Okta, Slack) by the relevant Departments. Information about application & system deprovisioners can be found on the [Tech Stack Applications](/handbook/business-ops/tech-stack-applications/) handbook page.
    - **Important** When submitting the form please ensure to use PST format, even applicable to team members located in EMEA and APAC regions. See the below as a guide or alternatively [this conversion chart](https://24timezones.com/difference/amsterdam/pacific): For Example, If a team member is located in Japan their offboarding issue will be created at 04:00 PM their regional time (APAC, EMEA, AMER) or 12pm their regional time on Friday. on their last day of employment.
1. **PBP**: The PBP will also inform the team member that they will receive an invitation in the next 48 hours (After Offboarding form has been submitted) from CultureAmp to complete an exit survey. 
1. **PBP**: PBP will forward the resignation email to the People Experience team at `people-exp@gitlab.com`, the People Operations Specialist team email inbox `peopleops@gitlab.com`, as well as to the payroll lead. The PBP will indicate the last day, reason for resignation, and rehire eligibility in the email. 
_Note: If the team member has a contract with a co-employer, the payroll lead will forward the email to the contact at the co-employer._
1. **Experience**: The People Experience will save a pdf of the resignation email to the team member's BambooHR profile in their `Contracts & Changes` folder. 
1. **Specialist**: The Specialist handling the exit interview / survey process will add their name to column `AC` in the offboarding [People Exp/Ops Tracker](https://docs.google.com/spreadsheets/d/1Z45eOZ2rCRIKgGae3eOKQ8lhIbAPikSs5gLz3V8Kh9U/edit?ts=5e18b512#gid=488708224) as a DRI, or ping the other Specialists to be certain that there is a fair balance and rotation of who is the DRI.

1. **Experience**: The People Experience Team will determine who will create the offboarding issue according to the [offboarding guidelines](/handbook/people-group/offboarding/offboarding_guidelines/), and confirm this in the [People Exp/Ops Tracker](https://docs.google.com/spreadsheets/d/1Z45eOZ2rCRIKgGae3eOKQ8lhIbAPikSs5gLz3V8Kh9U/edit?ts=5e18b512#gid=0).
    
    * _Note: For resignations from Director-level and above, or for departures that have significant context pre-dating the exit interview that can help make the discussion more effective, the [People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) will send out the exit survey and conduct the exit interview._     
    
1. **Specialist**: 

- Regrettable Offboardings:

The People Specialist will review the team members calendar and add an exit interview event to their calendar. The invite will include a message about how this interview is optional. A brief training on how to start and conduct exit interviews can be found [here](https://docs.google.com/presentation/d/1QCd6QLKcgCEzncJ2zdeDsnA6zlWFAmZre69Q1LKycJ0/edit?usp=sharing) (internal only)
      * An offboarding email will be sent [automatically](https://about.gitlab.com/handbook/people-group/engineering/offboarding/#voluntary-offboarding-email) to the team member once the offboarding form has been submitted. 
      * The assigned People Specialist will then send out the Exit Survey via Culture Amp (using the Exit Survey template in Culture Amp) and communicate with the departing team member to mention that they would like to schedule a non-mandatory exit interview, to occur during the team member's notice period.
      * To send the Exit Survey, log into Culture Amp and click on `Surveys` from the top menu bar. Select `Exit Survey` and select the `start` exit button - this will allow the People Specialist to assign the survey to the departing team member.
      * Review the team members calendar and add an invite for an exit interview. In the event description add the following [note](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/blob/master/.gitlab/email_templates/exit-interview-template.md). This is to ensure the team member knows this interview is optional. 
      * Remind the departing team member it is necessary to complete the [Exit Survey](#exit-survey) _prior_ to the Exit Interview so it can be discussed.
      * Before the exit interview the interviewer (The People Specialist and/or People Business Partner) should make sure to carve out time to review the Exit Survey answers and prepare for the interview. If any voilations to our Code of Conduct are mentioned in the exit survey, these should be discussed first in the exit interview. 
      * Once the exit interview has been completed and all notes entered, the assigned People Specialist will ping the PBP in the `#pbp-peopleops` Slack channel to confirm that the notes have been added and that the Exit Interview has been completed. This ping will occur within 48 hours of the exit interview.

- Non Regrettable Offboardings:
 * Unless otherwise directed to follow the Regrettable offboarding process, an offboarding email will be sent [automatically](https://about.gitlab.com/handbook/people-group/engineering/offboarding/#voluntary-offboarding-email) to the team member once the offboarding form has been submitted. 

1. **Experience**: The People Experience team member will reach out to the departing team member directly to confirm the time at which they can proceed with the creation of the [offboarding issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/offboarding.md) on the team member's last day. **Offboarders will see issues being created automatically at the latest 4:00pm local regional time, at which point de-provisioning will commence, unless there are unavoidable circumstances for a later offboarding.** 

### Updating Key Offboarding Dates
In the case an offboarding date needs to be updated, follow the next steps. 
1. Make a comment in the appropriate thread in the `#offboarding` channel. 
1. Ping the `@people-exp` to update the following places. 
   1. Offboarding tracker 
   1. BambooHR 
   1. Notify Payroll of the changes 
1. Save the email confirmation of the new offboarding dates in the `Termination` folder. 

### Exit Survey

The Exit Survey provides those team members being offboarded with the opportunity to freely express views about working at GitLab.
People Operations Specialists will send the [CultureAmp](https://www.cultureamp.com/) survey to the appropriate team members who are leaving GitLab voluntarily. 

The People Specialist and the team member may proactively set up time to discuss their responses and ask for further information. Exit interviews are not mandatory, however input will help provide GitLab with information regarding what is working well and what can be improved. Please point team member to the [Offboarding FAQ page](/handbook/people-group/offboarding/faq.html).

Should you want to provide feedback and are not given the opportunity for an interview, please feel free to send your feedback to the People Business Partner for your division, or Team Member Relations. 

#### Tasks for People Operations

1. Monitor the #offboarding slack channel to be notified of leavers
1. If the People Operations Specialist was selected as the exit interview owner and the offboarding is regrettable, add a spiral pad emoji to signal you are processing this exit interview
1. Schedule a 30 minutes zoom call with the leaving team member within 1 week of their exit date.
Email invite template:
"I wanted to schedule time with you to go over the exit interview questions that were sent to you via Culture Amp.  This meeting is optional, if there is additional information that you would like to share please accept this invite or propose a new time.
If you do not wish to have an in person exit interview you can simply decline the meeting.
Thanks and I wish you the best in your future endeavors!"
1. Log into CultureAmp via Okta > Surveys > Exit Survey > Start exit > select team member > select your name > set dates with survey to be completed two days before interview > send
1. Populate columns 'Exit Interview - POPS' with your name and 'Exit Interview Date' of the [Offboarding tracker](https://docs.google.com/spreadsheets/d/1Z45eOZ2rCRIKgGae3eOKQ8lhIbAPikSs5gLz3V8Kh9U/edit?ts=60c1df26#gid=330541891)
1. Complete the exit interview by asking questions to add context to survey answers and save your notes in CultureAmp. Click complete on the bottom of the page once done.
1. Post in #pbp-peopleops: Hi 'applicable PBP' ‘Emp name’ exit survey is available for review now. (Flag trends, red flags etc.)

#### Exit Survey - Inactive Team Member
Follow this process to send out an exit survey to an inactive team member;

1. Log into CultureAmp > Settings > Users.
1. Search for the former team member in the inactive list. 
1. Select the `activate` user option - This will add them to CultureAmp until the next data sync. The sync runs daily at 1 am PDT. 
1. Update the email address to the team member personal email. 
1. Search the former team member in the active list and select `send invite` on the right side of their name.
1. Open the Exit Survey and follow normal exit survey steps. 

#### Exit Survey Reporting on Culture Amp.(For Culture Amp Admins)

Culture Amp Exit Survey now has an additional "Reporting to" demographic. Initially, for reporting purposes we were using the "Manager" demographic which is no longer used on our account, i.e the data that syncs across from BambooHR is the "Reports to" field. However, this was not enabled on the actual exit survey. The "Manager" demographic was disabled on 19th November 2019. For reporting purposes, Culture Amp admins should use the "Reporting to" demographic while running reports on Culture Amp for team members who left after 19th November 2019 and the "Manager" Demographic for team members who left prior to that date.

## GitLab Alumni Program

All offboarding may request to be added to the Slack channel `gitlab-alumni` by requesting through the PBP who can review the request and confirm to the People Experience Associate who can open an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=slack_googlegroup_1Passwordgroupvault) on behalf of the former team member.
Reasons that someone would not be permitted to join would be:
- Due to involuntary offboarding for extreme behavior or a violation of our [Code of Business Conduct and Ethics](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d).
- Underperformance or other causes for involuntary offboarding are not a reason to exclude someone from the alumni program. 
- Team members who have less than 6 months of tenure with the organization.

The purpose of this channel is to network and socialize with team members.
Joining the channel is voluntary and subject to GitLab's [Code of Conduct](/community/contribute/code-of-conduct/).

GitLab, the company, monitors the channel and can remove people from it at their sole discretion.
The GitLab [Code of Business Conduct and Ethics](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d) is enforced in the channel.

## Involuntary Offboarding

Involuntary offboarding of any team member is never easy. We've created guidelines and information to make this process as humane as we can. Beyond the points outlined below, make sure to refer to our guidelines on [underperformance](/handbook/leadership/underperformance), as well as the [offboarding issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/offboarding.md).

If the need for Involuntary offboarding arises, the process is as indicated below:



### Involuntary Process

The manager and the team member should have walked through the guidelines on [underperformance](/handbook/leadership/underperformance) before reaching this point.

1. ***Manager***: Reach out to the their TMR specialist for assistance.
    * TMR will ask about what the performance issues have been, how they have been attempted to be addressed, and review all manager/team member documentation.
    * Once the review has been completed and the decision has been made to offboard the team member, the manager should review the best timing for the involuntary offboarding call and termination date. We recommend to avoid scheduling a involuntary offboarding call while a team member is scheduled for any sensitive customer meetings or is on-call, per guidance below under Last working day. 
    * TMR will partner with the People Specialists to coordinate the offboarding of the team member.
    * TMR will inform the People Business Partner on the possible upcoming offboarding. 
1. ***TMR***: The TMR will notify the People Operations Specialist team of the offboarding by posting in the `#Offboardings` confidential Slack channel. The TMR will indicate the team member's name, last day, reason for resignation, and rehire eligibility. The first Specialist to respond to the post will be the Specialist that partners with the TMR for the offboarding process.<br>
_Note: If the team member has a contract with a co-employer, the payroll lead will forward the email to the contact at the co-employer._
1. ***TMR***: The TMR will create a private Slack channel that will include the PBP, Manager, and Leader of the organization to review the offboarding and agreed upon offboarding date. 
1. ***Specialist***: If applicable, the TMR will prepare the severance agreement in preparation for the call between the TMR specialist, Manager, and team member. Additional guidelines for the preparation of this agreement can be found below in the [Separation and Release of Claims Agreements](#separation-agreement) section. To determine whether or not a severance agreement is applicable, please refer to the `Severance Elgibility` guidelines accessible by PBPs, and Team Member Relations Specialist.
1. ***Manager***: Once the date and time is confirmed, the manager will schedule time with the team member and send the TMR specialist a _private and separate calendar invite_ with the Zoom details for the meeting with the team member to share the news of the offboarding. 
1. ***Payroll***: If the team member is employed by a PEO/entity with statutory leave requirements, review if any time off needs to be paid on the last paycheck by looking in BambooHR in the `Time Off` section.
1. ***People Experience Associate***: The PEA will confirm and coordinate the date and time of the offboarding and who will be available to assist with the offboarding. At this stage, the name of the departing team member is ***not*** yet shared. To share this information, the People Business Partner will complete the [Offboarding Form](https://forms.gle/HzUAVCgTJ4v5HSSA9). Once submitted a summary of the response will be posted to the `#offboarding` Slack channel. 
 
1. ***TMR/Manager***: Discuss best mode of communicating the bad news to the team member. This discussion can happen via a private chat-channel, but it is best to be done via video.
1. ***TMR/Manager***: Decide who will handle which part of the conversation, and if desired, practice it.
    * If needed the TMR will provide the manager with a script for the offboarding meeting. 
1. ***TMR/Manager***: If the team member who is being terminated is a people manager, a _communication plan for the team_ regarding the departure should be in place before the offboarding proceeds. The communication plan should include: identification of an interim leader, (ideally) a scheduled meeting with the interim leader for the direct reports, a scheduled team call to announce the current manager's departure and the interim leadership, and finally an announcement in the `#team-member-updates` Slack channel to share with the wider team. 
   * Important: 
      * Informing the team and answering questions should be the top priority.
      * No announcement should be made in the `#team-member-updates` Slack channel until the team call has been completed.
      * In most cases, a team call can occur the same day of the offboarding. If necessary, the offboarding can be announced in the `#team-member-updates` Slack channel the following day.
1. ***TMR/Manager***: Decide what offboarding actions need to be taken _before_ the call (e.g. revoke admin permissions), or _during_ the call (e.g. revoke Slack and Gmail access), and which ones can wait until later. You can reference the [offboarding issue template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/offboarding.md) for the full list of actions. This conversation should take place in the private Slack channel with PBP, Manager and Leader.
1. ***TMR***: If the team member is a risk to the production environment, the TMR should privately reach out to the `Infrastructure Managers` to determine who will be available to assist with the offboarding. Once an infrastructure team member has been identified, they should be added to the private calendar invite sent to People Experience, Security, and Payroll to hold the time for the team member offboarding. Once the offboarding conversation starts the TMR will privately Slack the infrastructure contact the name of the team member to start the offboarding process.

When on the call...

1. ***Manager***: Deliver the bad news up-front, do not beat around the bush and prolong the inevitable pain for everyone involved. The Manager will make it clear that the decision is final, but will also explain what led to this decision and will point to the process that was followed to reach this decision. A sample leading sentence can be: 

_"Thanks for joining the call, [team member name].
Unfortunately, the reason I wanted to speak with you is because we have decided that we have to let you go and end your employment / contract with GitLab because of xyz."_

1. ***TMR***: As soon as the conversation begins, the TMR will complete the offboarding form which will post to `#offboardings` Slack channel with the name of the team member to immediately start the offboarding process.
1. ***People Operations Specialist***:The assigned People Specialist send out the Offboarding Informational Email according to the appropriate template. Current templates include [USA](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/email_templates/Offboarding_Involuntary_USA.md),[Canada](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/email_templates/Offboarding_Involuntary_Canada.md) and[Other countries](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/email_templates/Offboarding_Involuntary_Other_countries.md)
1. ***Manager***: Hand the call over to the TMR to continue. 
1. ***TMR***: The TMR will also make it clear that the decision is final, but also will genuinely listen to the team member's side of the story since there may be useful lessons in what they say for the rest of the team e.g. regarding hiring and vetting practices.
1. ***TMR***: Make sure to communicate the [practical points](#offboarding-points) from the offboarding memo outlined below.
1. ***TMR***: Once the conversation is complete, the TMR will stage the severance document in DocuSign for review and signatures.

#### Last Working Day 

After the involuntary offboarding call has taken place and the last working day has been determined, team members will have no access to GitLab systems and may not be required to do any work on GitLab's behalf. If they are on "Garden Leave" they will still be active on payroll through the termination date. When determining the timing of the involuntary offboarding call and termination date it is important to consider any effect this might have on ongoing tasks and responsibilities of the team member. As a manager, in collaboration with the Team Member Relations Specialist (TMR) and/or the People Business Partner (PBP), we recommend to avoid scheduling the involuntary offboarding call while a team member is scheduled for any sensitive customer meetings or is [on-call](/handbook/on-call/). If this is unavoidable, the manager is responsible for ensuring a transition/remediation plan. 

The People Experience team will generate the offboarding issue at the end of the team member's last working day, as per notification from the Team Member Relations Specialist (TMR) and the People Business Partner (PBP). Once the `Last Working Day` or Garden leave expires the team member will be officially offboarded from GitLab.  Prior to the offboarding issue and the overall process for the term listed below.  

`Last Working Day` - Only for US Team members

*  ***TMR***: TMR will have a legal/CPO review of the planned offboarding.
*  ***TMR***: TMR will inform payroll, compensation and benefits, security and the stock administration of the date the team member will have access suspended and the official offboarding date prior to the start of the official offboarding issue.  The TMR can either inform the group via the confidential #offboardings channel 1-2 days prior to the scheduled exit or via a zoom call.  
*  ***TMR***: TMR will work with the People Experience Team to ensure the offboarding issue has the correct dates and all GitLab offboarding team members in payroll, compensation and benefits, security and stock administration have been communicated to and understand the correct offboarding date. 

_Note: The offboarding process will remain the same as listed below in the `Involuntary Process` section below. The People Specialist will notify the team via the confidential #offboardings Slack channel to start turning off the team member's access._ 
                                                                
### Hold for In-Vol Offboarding
This form is used to help coordinate all necessary stakeholders for an involuntary offboarding. This will allow all stakeholders to be on standby to offboard the team member effeciently to prevent any possible security threats. 

This form is located in the Offboarding Channel shortcuts. 

- **Date of Offboarding**
- **Time of Offboarding PST**  
- Location
- Anything else we should know 

## Last Minute Offboardings
1. **People Operations** (Experience & Specialists): If the offboarding is requested on the same day as the termination date, ensure the time of the offboarding creation on the spreadsheet is after the time submitted. You may need to adjust it to ensure the automation picks up the entry. For example: An offboarding form is submitted at 5:00 pm PST - the offboarding creation time should be no earlier than 5:00pm PST. 
1. Terminate the team member in BambooHR 
1. Fill in Exit Impact 
1. Intiate the Okta import 
   -Okta entitlements are driven based on BambooHR status. Therefore to remove the user in Okta, the best way to set off that workflow is to force an Import from BambooHR into Okta. To do this, log into the Okta dashboard and select Admin in the upper-right hand side of the page. Once on the Okta Admin dashboard, select Applications, search and select BambooHR Admin, go to the Import tab and click the Import Now button. This will force an import, and you should see a message that a user has been removed after processing completes. (This will not be the case if the team member is placed on Garden Leave).

## Critical Points During Offboarding Call
{: #offboarding-points}

The following points need to be covered for any team member:
1. Final Pay: "Your final check (or invoice period) is for the pay period of X and includes X days of pay”.
1. Company Property: “Please return all property as explained in the handbook, also please delete GitLab’s email connection from your phone”.
1. Business Expenses: “Please create your final expense report to Expensify (for employees), OR, please file any outstanding expenses with your final invoice (for contractors), so these can be reimbursed to you in a timely manner”.
1. Confidentiality and Non-Disclosure: “We know you are a professional, please keep in mind the agreement you signed when you were hired”.
1. If you would like GitLab to share your personal email with the rest of the company, please send an email to People Ops or a farewell message that can be forwarded on your behalf.

The following points need to be covered for US-based employees:
1. COBRA: “Your benefits will cease on last day of the month you are eligible for Consolidated Omnibus Budget Reconciliation Act (“COBRA”), the carrier (Lumity) has been notified and the carrier will send out the paperwork to your home address on file”.
1. PPACA: "You may also be eligible under the Patient Protection and Affordable Care Act (“PPACA”) for subsidized health care options via the marketplace.
If you are interested it is important that you sign up with the market place well before the 15th of the month to have coverage for the following month”.
1. HIPAA: " Under the Health Insurance Portability and Accountability Act of 1996 (HIPAA), if you need a certificate of credible coverage please download it from your current carrier's online portal or request it from People Ops”.
1. Unemployment insurance: "It is up to your state's labor agency (in CA: EDD) to decide if you are eligible for unemployment insurance”.
1. Please remember to keep GitLab informed: "If you move I want to be sure your W-2 gets to you at the end of the year.
You may also contact X at GitLab (provide phone number and email address) with any other questions that you may have" (consider inviting them to contact you at anytime for any reason).

### Sample Offboarding Memo

If appropriate (to be determined by conversation with the manager, the Group Executive, and People Ops), use the following [offboarding memo](https://docs.google.com/document/d/11Uk8p4VJrLnDD5IAtbTwswPUUEnmeEOazS1kJMhOu70/edit?usp=sharing), which is provided here as an openly viewable Google Doc, but of course needs to be personalized and tailored to each individual's situation.
As written, it is applicable to US-based employees only.

### Separation and Release of Claims Agreements
{: #separation-agreement}

Separation and Release of Claims Agreements ***do not*** apply for all offboardings. To review in which cases they do/do not apply, please reference the `Severance Eligibility` document accessible by People Specialists and PBPs. In the case that a severance agreement is applicable, the steps below should be followed:

### Severance Process for US-based Team Members

1. ***TMR***: The TMR Partner/Specialist assigned to the particular offboarding case at hand should select the appropriate severance template. Options include: Non-California over 40, California over 40, Non-California under 40, California under 40. 
1. ***TMR***: The TMR Partner/Specialist should make a copy of the template and save it in the `Copies of Individual Severance Agreements` folder. 
1. ***TMR***: The TMR Partner/Specialist should fill out the document. 
1. ***TMR***: The TMR Partner/Specialist will ping the stock team in the stock option section of the document. 
1. ***TMR***: The TMR Partner/Specialist will share with the PBP and Legal for final review/approval. 
1. ***TMR***: The TMR Partner/Specialist will stage the document in DocuSign for signatures. Please note that depending on the template used, team members have a limited amount of time to sign the Separation and Release of Claims Agreement.


*  When staging the document for signatures, please note:
    *  Remember to send the document to the team member's _personal_ email address
    *  Select the `assign signature order` option in DocuSign to ensure the team member signs the document first
    *  All US (Inc.) severance agreements should be signed by Pattie Egan, VP of People Operations.  If Pattie is unavailable then Wendy Nice Barnes, CPO will be responsible for signing.
    * US (Federal LLC.) severance agreements need to be signed by an appropriate team member within the Federal Entity. 
    
1. ***TMR***: When the signed document is received, the TMR should upload it to the team member's BambooHR profile in the `termination` folder
1. ***TMR***: As a final step, the TMR specialist will email the appropriate uspayroll@gitlab or nonuspayroll@gitlab and total rewards that the final severance agreement has been uploaded to the team members BambooHR Folder under signed documents.  

_Important Notes:_
* Separation pay is not paid until the ex-team member signs the document and the revocation period has passed.
* We treat team members equally and therefore do not take tenure into consideration when determining separation pay unless legally required. We know that other companies sometimes gives higher separation pay to longer tenure. We think that a short tenure can be harder to explain to a next employer and with a shorter tenure you might have less stock option upside, maybe you have not even reached your vesting cliff.

* Make sure you understand the rules for over 40.
* You must use language in your exit meeting that is in no way forceful of the ex-team member to sign the agreement. Use phrasing such as “if you choose to sign”; “you have a right to have legal council review this document before you sign”, etc.


### Team member leave during an investigation

For team members who will be placed on leave during an investigation please follow the process below:  

1. **TMR**: Communicates with Legal, Aligned PBP regarding the decision to place a team member on leave and disable accounts during an investigation.
1. **TMR**: TMR completes the Suspension bot in the '#offboarding' slack channel to request IT and People Ops support for disabling a team members accounts. This can be found by selecting the lightning bolt or shortcut button in slack in the `#offboarding` channel. Once you complete the form, a message will post to the slack channel, in this message thread you will find a message with a link to another form. This form is only needed/used when a team member is returning from leave. 
1. **TMR**: TMR coordinates with IT and the People experience team in the same manner as an involuntary term to be able to disable accounts when a team member is informed of the decision.
1. **TMR**: TMR schedules a call with the teammember and manager to inform them of the decision to place the team member on leave while an investigation occurs.
1. **TMR**: TMR informs the manager and PBP when the conversation is completed and accounts are disabled.  During this period managers will not have access to the team members accounts.
1. **TMR**: TMR notifies IT that an Out Of Office message needs to be set on the account and include the message that the team member is OOO and please reach out to their manager.. 
1. **TMR**: TMR send the team member to their personal email the "Team member Suspension letter" via DocuSign.  The TMR will upload this document to the team members BambooHR file in the performance folder.
1. **TMR**: TMR once the investigation is completed the TMR will inform the aligned PBP and manager of the final decision.  If the team member is to be returned to work the TMR will reach out and schedule time to meet with the team member. 
1. **TMR**: If the team member is returning to work the TMR complete the suspension bot (The form will be located in the orginal slack thread) to notify IT and People Ops that the team member will be returning from leave and the date that accounts should be enabled.

For more information please review the handook page regarding the [Leave during investigations](https://about.gitlab.com/handbook/leadership/underperformance/#team-member-leave-during-an-investigation).

#### Process for Team Members Outside of the US

_To be documented_ 

## Communicating Departures Company-Wide

As explained briefly in the [offboarding issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/offboarding.md), GitLab does not provide any context around why people are leaving when they do.
However as mentioned in the procedures, for [voluntary offboarding](/handbook/people-group/offboarding/#voluntary-offboarding), the team member can work with their manager on sharing a company-wide message about their departure. 

Once the news has been shared with the team member’s team and other stakeholders, and messaging is agreed upon between the departing team members and their manager, the departing team member’s manager shares the following message company-wide about the team member’s upcoming departure in the `#team-member-updates` Slack channel. 
Managers can use this template as a guide on how to communicate a team member’s upcoming departure:

```
I want to share that [team member’s name] ([group] [role]) will be leaving GitLab, and [his/her/their] last day is (date of their last day). (Context to add about the team member’s time at GitLab - examples: their favorite contribution to the handbook or they can use the update to express gratitude towards teams and individuals that have made their experience at GitLab positive.) I would like to take this opportunity to thank (team member's name) for their contributions and wish them all the best for the future. If you have questions about tasks or projects that need to be picked up, please let me know. If you have concerns, please bring them up with your manager. To keep in touch with (team member's name) he/she/they can be reached at (contact info - e.g. LinkedIn, email, etc.)
```

If someone is let go involuntarily, this generally cannot be shared since it affects the individual's privacy and job performance is intentionally kept [between an
individual and their manager](/handbook/communication/#not-public).


If you are not close to an employee's offboarding, it may seem unnecessarily swift.
Please remember that these decisions are never made without following the above process to come to a positive resolution first - we need to protect the interests of the individual as well as the company, and offboarding is a last resort.
According to our values [negative feedback is 1-1 between you and your manager](/handbook/values/#negative-feedback-is-1-1) and we are limited in what we can share about private employee issues.
Please discuss any concerns you have about another employee's offboarding with your manager or your People Business Partner.

Given the expectations and responsibility that come with a VP and above position, when there is an involuntary offboarding for one of these positions, additional context for the personnel change can be provided to the organization.  

Silence is unpleasant. It's unpleasant because we are human, which means that we are generally curious and genuinely interested in the well-being of our team members.

Is it _more_ unpleasant in a remote setting? Probably not.
When people are all in the same office building, they can "kinda sorta" know what may be coming because of the grapevine, the water cooler, and so on. When the news hits it might be less of a shock - only because of unprofessional behavior in the first place.
But at larger companies with multiple office buildings, departures will tend to come as more of a surprise and with less context (at least to the people in other buildings).

## What Do We Share?

We strive to maintain personal information regarding all team members private, this includes information regarding a team members voluntary or involuntary departure from GitLab.
However, a manager with the consent and approval of the departing team member can share more details with the GitLab team regarding the decision to leave GitLab.

For a voluntary departure a team member may have chosen to leave for many different reasons, career development, promotion, a new role or career path, dislike remote work, etc.
For example, a team member may tell their manager that they really miss being in an office environment and remote work is not suitable for their personality.
Based on that decision they have taken another opportunity that allows them to go to an office.

If the departing team member gives their manager permission to share that information then the manager will share while making the departure announcement on the team call.
We want all GitLab team-members to be engaged, happy and fulfilled in their work and if remote work, the requirements of the job or the role it self are not fulfilling we wish our team members the best of luck in their next endeavor.

Regarding involuntary offboarding, certain information can also be shared with the GitLab team regarding the departure.
Some team members do not thrive or enjoy the work that they were hired to do.
For example after starting at GitLab a team member quickly learns they have no desire or interest to learn Git or work with Git.  This doesn't make them a bad person, it just means they don't have the interest for the role and based on that the decision was made to exit the company.
Again, we want all team members to enjoy and thrive in their work and that may or may not be at GitLab.

The departing team member may work with their manager to author a goodbye message for voluntary offboarding:
  1. Work with your manager on the message that your manager will share in `#team-member-updates` on Slack. 
  2. Send it to your manager for approval. If, as a manager, you are in doubt about the message - please reach out to your manager or [aligned People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) for additional review.
  3. Managers are encouraged to use the suggested template (above link) and have the space to add more color wherever they see fit.

In some instances there will be no further clarification on why a team member has departed, if there are concerns you can address those with your manager.
Different levels of transparency will exist based on maintaining respectful treatment for all departures.
Having team members leave may be a learning opportunity for some, but should not be a point of gossip for anyone.
Managers will need to balance the opportunity for learning with the expectation of privacy and consult their People Business Partner should they have questions.

Transparency is one of our values.
In the case of offboarding transparency [can be painfully specific, calling out an employee’s flaws, while inviting more questions and gossip](https://outline.com/PTGkER).
We opt to share the feedback only with peers and reports of the person since we balance transparency with our value of collaboration and negative is 1-1.

## Turnover Data

GitLab's [turnover data](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6251791&udv=904340) is only viewable internally.


## Managing the Offboarding Tasks

#### Offboarding Issue
To track all tool deprovisioning, please open an offboarding issue following the [offboarding guidelines](/handbook/people-group/offboarding/offboarding_guidelines/).

#### Returning Property to GitLab

As part of offboarding, any GitLab property valued above 1,000 USD needs to be returned to GitLab. 

For laptops, please refer to the [Laptop Buy Back Policy](/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptop-buy-back-policy) which states that team-members have the option to buy back their existing laptops either when it gets refreshed for a new one, or when the team member is offboarding. If the team member has completed 1 calendar year or more at GitLab at the time of laptop refresh or offboarding, they can opt to keep their laptop at no cost. If the team member hasn't completed 1 calendar year at GitLab at that time, they have the option to purchase their laptop for current market value.

To return your laptop to GitLab, please contact itops@gitlab.com immediately upon offboarding.

#### Expensify
This section of the Accounting Department.

To remove someone from Expensify Log in to [Expensify](https://www.expensify.com/signin) and go to "Settings" in the left sidebar.
Select the right policy based upon the entity that employs the team member. Select "People" in the left menu.
Select the individual's name and click "Remove".
If the person has a Corporate Credit Card assigned to them, please notify Accounts Payable before un-assigning it.

### Retrospective for Managers
For involuntary offboardings it is optional to do a retrospective on the hiring, onboarding and coaching/communication of the departing team member. As a manager, you can use [this template](https://docs.google.com/document/d/1AWth5o_sagDTwQ92FJr7WDHPNezEg6-BgRY6Tafejgo/edit?usp=sharing) for a retrospective. Please share the filled out template with your manager as well as the [People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) for your group.

Within the Engineering division this is a **required process** because it causes hiring managers to reflect on what led to the ultimate decision of parting ways with the team member, and how that might be prevented during future hiring processes.

#### Evaluation

1. How could this outcome have been avoided?
2. Were there early signs that were missed?
3. In retrospect, what questions should have been asked to bring awareness and
  ownership to performance issues?
For example, "How would you compare yourself relative to your peers?"
People are surprisingly honest here.

## Declination (Prior to Day 1 and up to Day 5)

Note: Prior to start date- If a team member has been added to BambooHR and the Payroll Changes Sheet but does not start, their profile will be deleted from BambooHR. Notify the Payroll Team accordingly and make a note against their particulars in the Payroll Changes Sheet. 

Should a new hire be onboarded by the People Experience team, but: 

* Communicate very close to their start date that they have declined to continue, or
* Decline on their first day, or within their first 5 days, then

This is not seen as a true offboarding, but there are still relevant steps to take.

1. The People Experience Associate will manually create a skeletal [offboarding issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/offboarding.md) and only include deprovisioning tasks that are applicable to the access that was given up to that point.
1. The People Experience Associate will inform the [team member with ownership rights](https://about.gitlab.com/job-families/people-ops/people-operations/#director-global-people-operations) of BambooHR to delete the team members' BambooHR profile entirely by messaging them in the private People Ops Slack channel.

## PEO Offboarding Guidelines

As part of the [country conversion process](/handbook/people-group/employment-solutions/#country-conversions), the People Specialist team obtains information from our PEOs in each country regarding offboarding processes and requirements.

## US Unemployment Claim Management

In the United States, unemployment insurance provides benefits to GitLab team members who have lost their jobs through no fault of their own. The purpose is to provide temporary financial assistance to employees who meet certain requirements. Unemployment insurance is administered at the state level in compliance with federal law. Each state establishes its own rules with respect to amounts, duration and eligibility for unemployment insurance.

Unemployment insurance is funded by employer contributions. Most states also have state unemployment taxes. With a few exceptions, employers typically pay the state unemployment taxes as well. Individual states establish eligibility requirements for unemployment benefits. To be eligible, employees are required to meet state requirements for earnings and for time worked during the base period. In some cases, the state may ask for more extensive wage details on a claim. 

The People Operations Specialist team own the US Unemployment Claim process. Ashley Jameson is the GitLab US Unemployment Claim Designate since she is US-based, for this US process and she can be reached at `peopleops@gitlab.com`.

When unemployment claims are received, usually by [company mail](/handbook/eba/#company-mail), they will be sent and reviewed by the GitLab US Unemployment Claim Designate. The designate will:

- Review all claims and protest when appropriate to help minimize increases to GitLab's
unemployment rate, according to the [People Ops internal handbook](https://gl-people-operations.gitlab.io/internal-handbook/) process.
- Audit quarterly unemployment reports to ensure that claims are valid and that charges are contested, when appropriate.

### Reporting a False Unemployment Claim

If you are a full-time team member and you are contacted by your state's Unemployment Commission to discuss your request for unemployment benefits, you may be a victim of unemployment claim fraud. Before giving out any information to the caller, please confirm that you are speaking with an agency employee. If you confirm with your state's Unemployment Commission that there is a fraudulent claim, please report it via email to `peopleops@gitlab.com`. Additionally here is a link to the [U.S Department of Labor Report Unemployment Insurance Fraud](https://www.dol.gov/agencies/eta/unemployment-insurance-payment-accuracy/report-unemployment-insurance-fraud), which lists additional information.
