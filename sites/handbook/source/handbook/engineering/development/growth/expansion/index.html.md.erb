---
layout: handbook-page-toc
title: Growth:Expansion Group
description: "The Growth:Expansion group works on feature enhancements and growth experiments across GitLab projects"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

The Expansion Group is part of the [Growth Sub-department]
and works with our product counterparts on [Growth priorities](/direction/growth/#growth-kpi).
The engineering effort consists of developing and running growth experiments on [GitLab.com](https://www.gitlab.com/), adding and enhancing features, and improving the quality of our code across multiple GitLab projects. We track the delivery of both [Deliverables] and [Growth-Deliverables].

See [What's next](/direction/growth/#3-whats-next-growth-group-issue-boards)

* I have a question. Who do I ask?

Questions should start by @ mentioning the Product Manager for the [Expansion group](/handbook/product/categories/#expansion-group)
or creating a new issue in the Growth Product [Expansion issues] list.

## Mission

The Growth:Expansion Group works on product priorities and [growth deliverables](https://about.gitlab.com/handbook/product/growth/#growth-deliverables)
with our [Growth product managers](/handbook/product/growth/)
and contributes to Growth initiatives including the [Registration Flow](https://gitlab.com/groups/gitlab-org/-/epics/4745) 
and [Continuous onboarding](https://gitlab.com/groups/gitlab-org/-/epics/4817).

## Team Members

The following people are permanent members of the Growth:Expansion team:

<%= direct_team(manager_role: 'Fullstack Engineering Manager, Growth:Conversion and Expansion', role_regexp: /(Growth:Expansion)/) %>

### Group members

We work directly with the following team members in the Growth:Expansion group:

<%=
other_manager_roles = ['Director of Engineering for Growth, Sec, and ModelOps','Senior Engineering Manager, Growth']
direct_managers_role = 'Fullstack Engineering Manager, Growth:Conversion and Expansion'
roles_regexp = /Expansion/

stable_counterparts(role_regexp: roles_regexp, direct_manager_role: direct_managers_role, other_manager_roles: other_manager_roles)
%>

### UX
The Growth UX team has a [handbook page](/handbook/product/growth/) which includes [Growth specific workflows](/handbook/product/growth/#product-designer-assignments).

## Functional Counterparts

We collaborate with our colleagues in the Growth Sub-department teams:

* [Activation](/handbook/engineering/development/growth/activation/)
* [Adoption](/handbook/engineering/development/growth/adoption/)
* [Conversion](/handbook/engineering/development/growth/conversion/)
<!-- * [Expansion](/handbook/engineering/development/growth/expansion/) -->
* [Product Intelligence](/handbook/engineering/development/growth/product-intelligence/)

As well as the wider Growth Sub-department [stable counterparts](/handbook/engineering/development/growth/#stable-counterparts).

## How we work

* We're data savvy
* In accordance with our [GitLab values](/handbook/values/)
* Transparently: nearly everything is public
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

### Prioritization

Prioritization is a collaboration between Product, UX, Data, and Engineering.

* We use the [ICE framework](/direction/growth/#growth-ideation-and-prioritization) for experiments.
* We use [Priority](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels)
  and [Severity](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#severity-labels) labels for bugs.

### Workflow

We use the [Product Development workflow](/handbook/product-development-flow/) when working on issues and
merge requests across multiple projects.

While not all of the work we do relates to the GitLab project directly, we use
[milestones](https://docs.gitlab.com/ee/user/project/milestones/) for planning and to track [Deliverables], [Growth-Deliverables] and other enhancements.

#### Overview

| Board | Description |
| ------ | ------ |
| [Planning] |  This board shows Expansion team work that has been allocated to a particular milestone. |
| [Deliverables] | A subset of the milestone board shows issues the Product Manager has determined to be `Deliverables`. |

#### Working boards

* The validation track is where the Product Manager - usually in collaboration with `UX` - defines what the team should aim to deliver. Once complete, the Product Manager will move to `workflow::planning breakdown` (larger issues) or straight to `workflow::scheduling` for Engineering to pick up. If there is no Engineering input required the issue can be closed.
* The Retention Engineering group (`group::retention`) schedules issues for development in the build phase, based on the Product Managers priorities. For `bugs` and `security issues`, [Priority](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels) and Severity labels will have been added by the Product Manager.
* Combined workflow board shows the combined workflow including validation and build tracks.

| Board | GitLab.org | GitLabServices |
| ----- | ---        | ---            |
| Validation (PM/UX) | [All](https://gitlab.com/groups/gitlab-org/-/boards/1506660?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aexpansion) | [All](https://gitlab.com/groups/gitlab-services/-/boards/1509980) |
| Build (Eng) | [All](https://gitlab.com/groups/gitlab-org/-/boards/1506701?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aexpansion) | [All](https://gitlab.com/groups/gitlab-services/-/boards/1509985) |
| Combined | [All](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [All](https://gitlab.com/groups/gitlab-services/-/boards/1521362) |

The above boards can be filtered by milestone to provide additional context around priority.
For example a `priority::3` security issue ([due within 90 days](/handbook/engineering/security/#severity-and-priority-labels-on-security-issues))
will be added to one of the next 3 milestones by the Engineering team during the `workflow::scheduling` stage to ensure that SLA is met.

## Common Links

* `#g_expansion` in [Slack](https://gitlab.slack.com/archives/g_expansion) (GitLab internal)
* [Expansion issues]
* [Growth Sub-department]
* [Growth issues board]
* `#s_growth` in [Slack](https://gitlab.slack.com/archives/s_growth) (GitLab internal)
* [Growth engineering metrics]
* [Growth opportunities]
* [Growth meetings and agendas]
* [GitLab values]

[Expansion issues]: https://gitlab.com/gitlab-org/growth/product/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aexpansion

[Growth Sub-department]: /handbook/engineering/development/growth/
[Growth issue board]: https://gitlab.com/groups/gitlab-org/-/boards/1158847
[Growth issue list]: https://gitlab.com/groups/gitlab-org/growth/-/issues
[Growth engineering metrics]: /handbook/engineering/metrics/growth/
[Growth meetings and agendas]: https://drive.google.com/drive/search?q=type:document%20title:%22Growth%20Weekly%22

[Planning]: https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion
[Deliverables]: https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion&label_name[]=Deliverable
[Growth-Deliverables]: /handbook/product/growth/#growth-deliverables
